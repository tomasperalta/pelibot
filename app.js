const config = require('./config')
const Twit = require('twit')
const axios = require('axios')

// This will return the string with
// the recommended movie. If something
// goes wrong, returns an error message to tweet.
const getRecommendedMovie = async () => {
  const tmdbApiKey = config.movies_database_api_key // Your TMDb api key goes here
  const listPage = Math.floor(Math.random() * 234) + 1
  const minimumVotes = 50
  const minimumScore = 7
  const requestURL =
    'https://api.themoviedb.org/3/discover/movie?api_key=' +
    tmdbApiKey +
    '&language=en-US&sort_by=vote_average.desc&include_adult=false&include_video=false&page=' +
    listPage +
    '&vote_count.gte=' +
    minimumVotes +
    '&vote_average.gte=' +
    minimumScore

  // Does a GET request to the TMDb API
  // to get the random movie data
  const recommendedMovie = await axios
    .get(requestURL)
    .then((response) => {
      if (response.status === 200) {
        const moviesList = response.data.results
        const listLength = moviesList.length
        const randomIndex = Math.floor(Math.random() * listLength) + 1

        // Yayy! We've our random movie to recommend!
        const recommendedMovie = moviesList[randomIndex]

        // Now, let's start putting together
        // the string to tweet to the user
        const movieID = recommendedMovie.id
        const movieTitle = recommendedMovie.title
        const movieReleaseYear = recommendedMovie.release_date.split('-')[0] // We just want the year
        const movieURL = 'https://www.themoviedb.org/movie/' + movieID

        // We'll tweet this to the user
        const tweet =
          ' today you could watch ' +
          movieTitle +
          ' (' +
          movieReleaseYear +
          '). More info: ' +
          movieURL
        return tweet
      }
    })
    .catch(() => {
      return ' seems like something went wrong 💔. Try again in a few minutes!'
    })

  return recommendedMovie
}

// Our bot object
const bot = new Twit(config.twitter_api)
const myAccountId = config.twitter_user_id // Replace this with your Twitter account's ID

// Twitter's public stream
const stream = bot.stream('statuses/filter', { follow: myAccountId })

// Every time our account receives a tweet,
// the `stream` object will run a function
// to verify if the tweet it's a mention
stream.on('tweet', async (eventMsg) => {
  if (eventMsg.in_reply_to_screen_name === 'nosequever') {
    const userToMention = eventMsg.user.screen_name
    const toTweet = '@' + userToMention + ' ' + (await getRecommendedMovie()) // Remember getRecommendedMovie() it's asynchronous
    const tweetParams = {
      status: toTweet,
      in_reply_to_status_id: eventMsg.id_str,
    }

    // 🎉🎉 Tweets the recommendation 🎉🎉
    bot.post('statuses/update', tweetParams)
  }
})
